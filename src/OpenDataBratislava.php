<?php

namespace JasterStary\OpenDataBratislava;

class OpenDataBratislava {
  
  static $endpoint = "https://opendata.bratislava.sk";
  static $apikey = "";
  static $tmpdir = "";

  public function __construct($cfg = false){
    self::configure($cfg);
  }

  public static function configure($cfg){
    if (is_array($cfg)) {
      if ((isset($cfg['apikey']))&&(is_string($cfg['apikey']))) {
        self::$apikey = $cfg['apikey'];
      }
      if ((isset($cfg['tmpdir']))&&(is_string($cfg['tmpdir']))) {
        self::$tmpdir = $cfg['tmpdir'];
      }
    }    
  }
  
  public static function getCategories(){
    return self::getJSONResponse('/api/category');
  }

  public static function getCategoryInfo($id){
    $id = intval($id);
    return self::getJSONResponse('/api/category/'.$id);
  }

  public static function getCategoryDatasets($id){
    $id = intval($id);
    return self::getJSONResponse('/api/category/'.$id.'/datasets');
  }

  public static function getCategory($id){
    return array_merge(self::getCategoryInfo($id),self::getCategoryDatasets($id));
  }

  public static function getDatasetInfo($id){
    $id = intval($id);
    return self::getJSONResponse('/api/dataset/'.$id);
  }

  public static function getDatasetFiles($id){
    $id = intval($id);
    return self::getJSONResponse('/api/dataset/'.$id.'/files');
  }

  public static function getDataset($id){
    return array_merge(self::getDatasetInfo($id),self::getDatasetFiles($id));
  }

  public static function getFileInfo($id){
    $id = intval($id);
    return self::getJSONResponse('/api/file/'.$id);
  }

  public static function getFile($id, $store = false){
    $id = intval($id);
    return self::getResponse('/api/file/'.$id.'/download', $store);
  }
      
  public static function getResponse(String $path, $store = false){
    $ch = curl_init();
    $hdr = [
      'key: '.self::$apikey,
    ];
    curl_setopt($ch, CURLOPT_URL, self::$endpoint . $path);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $hdr);
    if ($store) {
      if ((!is_string($store))||(!is_dir($store))) {
        $store = sys_get_temp_dir();
      };
      $re = tempnam($store,"opendatabratislava-");
      if (!$re) return false;
      $fp = fopen ($re, 'w+');
      curl_setopt($ch, CURLOPT_TIMEOUT, 6000);
      curl_setopt($ch, CURLOPT_FILE, $fp); 
      curl_exec($ch); 
      fclose($fp);
    } else {
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $re = curl_exec($ch);
    };
    curl_close($ch);          
    return $re;
  }

  public static function getJSONResponse($path){
    return json_decode(self::getResponse($path),true);
  }
  
}
