# OpenDataBratislava

Reading open data api of city Bratislava

Just one trivial API reader.

For me to test Gitlab -> Packagist workflow.

## Installation

```

composer require jasterstary/opendatabratislava


```

## Config


```


return [
  "apikey" => 'get-your-own-api-key',
];



```

You can obtain API key here:
[https://opendata.bratislava.sk/en/page/openapi](https://opendata.bratislava.sk/en/page/openapi)


## Usage


```php

use \JasterStary\OpenDataBratislava\OpenDataBratislava;

OpenDataBratislava::configure(config('opendatabratislava'));
$categories = OpenDataBratislava::getCategories();
$category = OpenDataBratislava::getCategory(intval($_POST['category_id']));
$dataset = OpenDataBratislava::getDataset(intval($_POST['dataset_id']));

// in following case, file is downloaded, and file name returned:
$file_name = OpenDataBratislava::getFile(intval($_POST['dataset_id']), true);

// in following case, file is downloaded to $folder, and file name returned:
$file_name = OpenDataBratislava::getFile(intval($_POST['dataset_id']), $folder);

// in following case, file content is returned:
$file_content = OpenDataBratislava::getFile(intval($_POST['dataset_id']));


```

## License
MIT License.
